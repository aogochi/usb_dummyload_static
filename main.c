/*
 * File:   main.c
 * Author: aogochi
 *
 * Created on 2017/11/06, 13:19
 */

//RA0 ICSPADAT		:RA1 ICSPCLK	:RA2 NC			:RA3 MCLR		:RA4 NC			:RA5 NC
//RC0(AN4) VolumeIN	:RC1 OPA1IN-	:RC2 OPA1OUT	:RC3 OPA2OUT	:RC4 OPA2IN-	:RC5 NC


#include "mcc_generated_files\mcc.h"
#define drive_mA 100                                    //電流設定 0～100mA @FVR 1.024v
#define drive_set drive_mA/0.4                          // 0.4mA/bit       @FVR 1.024v

void main(void) {
    uint8_t ADvalue;
    uint16_t currentMAX = drive_set;                    //最大電流設定 0～255 1dig = 0.4mA @FVR 1.024v    
    SYSTEM_Initialize();

    DAC1CON1 = currentMAX;

    while(1){
        ADC_SelectChannel(4);
        ADC_StartConversion();
        while(ADCON0bits.GO_nDONE);
        ADvalue = ADRESH;                               //AD値の上位8bitを取得
        
        DAC1CON1 =(uint8_t)((currentMAX * ADvalue) >>8);   //設定電流変更
        __delay_ms(100);
    }
}
