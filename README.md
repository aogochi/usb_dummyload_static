# USB DummyLoad 定電流駆動版ファーム
[スイッチサイエンスさん](https://www.switch-science.com/catalog/3829/)や[Emerge+ショップ](https://shop.emergeplus.jp/usb-dummyload/)で販売している基板に標準で書き込まれているファームです。

基板のファームを変更後、元のファームに戻したい場合や定電流駆動をベースに動作条件を変更したい場合にご使用ください。